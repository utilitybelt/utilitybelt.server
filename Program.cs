﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using CommandLine.Text;
using CommandLine;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Logging;
using UtilityBelt.Networking;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace UtilityBelt.Server {
    class Program {
        static bool shouldExit = false;
        static DateTime lastclientAction = DateTime.UtcNow;
        internal static readonly long MAX_LOG_SIZE = 1024 * 1024 * 10; // 10MB
        public static string AssemblyDirectory => Path.GetDirectoryName(Assembly.GetAssembly(typeof(Program)).Location);

        public class Options {
            [Option('h', "host", Default = "127.0.0.1", HelpText = "The ip to listen on")]
            public string Host { get; set; } = "127.0.0.1";

            [Option('p', "port", Default = 21424, Required = false, HelpText = "The port to listen on.")]
            public int Port { get; set; } = 21424;

            [Option('v', "verbosity", Default = LogLevel.Information, Required = false, HelpText = "The log file verbosity. Can be one of: Trace, Debug, Information, Warning, Error")]
            public LogLevel LogLevel { get; set; } = LogLevel.Information;

            [Option('t', "timeout", Default = 0, Required = false, HelpText = "The default idle timeout to exit after when no clients are connected, in seconds. Set to 0 to disable.")]
            public int Timeout { get; set; } = 0;
        }

        static void Main(string[] args) {
            using (var mutex = new Mutex(false, "com.UtilityBelt.Server.InstanceCheck")) {
                bool isAnotherInstanceOpen = !mutex.WaitOne(TimeSpan.Zero);
                if (isAnotherInstanceOpen) {
                    Console.WriteLine("Existing because another instance is already running.");
                    return;
                }
                Parser.Default.ParseArguments<Options>(args)
                       .WithParsed<Options>(o => {
                           ILogger? logger = null;
                           try {
                               var serviceCollection = new ServiceCollection();
                               serviceCollection.AddLogging(builder => builder
                                   .AddConsole()
                                   .AddFilter((l) => l >= o.LogLevel)
                               );
                               var loggerFactory = serviceCollection.BuildServiceProvider()
                                   .GetService<ILoggerFactory>();

                               logger = loggerFactory?.CreateLogger("UBNetServer");
                               var server = new UBNetServer(new UBNetServerOptions(o.Host, o.Port), null);

                               _ = server.Start();

                               var exitTimeout = TimeSpan.FromSeconds(o.Timeout);
                               while (!shouldExit) {
                                   if (o.Timeout != 0 && server.Clients.Count == 0 && DateTime.UtcNow - lastclientAction > exitTimeout) {
                                       logger?.Log(LogLevel.Warning, "Exiting due to inactivity.");
                                       break;
                                   }
                                   else if (server.Clients.Count > 0) {
                                       lastclientAction = DateTime.UtcNow;
                                   }
                                   Thread.Sleep(100);
                               }
                           }
                           catch (Exception ex) { Console.WriteLine(ex.ToString()); logger?.Log(LogLevel.Error, ex.ToString()); }
                       });
            }
        }
    }
}
