﻿# syntax=docker/dockerfile:1

FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime
WORKDIR /publish
COPY /bin/Release/net6.0 .
EXPOSE 21424
ENTRYPOINT ["dotnet", "UtilityBelt.Server.dll"]
